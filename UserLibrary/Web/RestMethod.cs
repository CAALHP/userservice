namespace UserLibrary.Web
{
    public class RestMethod
    {
        // ReSharper disable InconsistentNaming
        public const string GET = "GET";
        public const string PUT = "PUT";
        public const string POST = "POST";
        public const string DELETE = "PUT";
        // ReSharper restore InconsistentNaming
    }
}