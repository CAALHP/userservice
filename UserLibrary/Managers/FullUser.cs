﻿using MarketplaceContract;

namespace UserLibrary.Managers
{
    public class FullUser
    {
        public User BaseUser { get; set; }
        public UserProfile Profile { get; set; }

        public FullUser(User baseUser, UserProfile profile)
        {
            BaseUser = baseUser;
            Profile = profile;
        }
    }
}
