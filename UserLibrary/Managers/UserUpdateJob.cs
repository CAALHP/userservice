﻿using System;
using caalhp.Core.Contracts;
using Quartz;
using UserLibrary.UserUpdate;

namespace UserLibrary.Managers
{
    public class UserUpdateJob : IJob
    {
        private readonly IHostCAALHPContract _host;
        private readonly IUserManager _userManager;

        public UserUpdateJob(IHostCAALHPContract hostCAALHPContract, IUserManager userManager)
        {
            if (hostCAALHPContract == null)
            {
                throw new ArgumentNullException("hostCAALHPContract");
            }
            if (userManager == null)
            {
                throw new ArgumentNullException("userManager");
            }
            _host = hostCAALHPContract;
            _userManager = userManager;
        }

        public void Execute(IJobExecutionContext context)
        {
            var userUpdater = new UserUpdater(_host, _userManager);
            userUpdater.Run();
        }
    }
}