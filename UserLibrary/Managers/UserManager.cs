﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using caalhp.Core.Contracts;
using caalhp.Core.Events.Types;
using Microsoft.Practices.Unity;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using Quartz.Unity;
using UserLibrary.UserUpdate;

namespace UserLibrary.Managers
{
    public class UserManager : IUserManager
    {
        private UnityContainer _container;
        public IServiceCAALHPContract ServiceImplementation { get; set; }
        public ConcurrentDictionary<string, FullUser> Users { get; set; }
        public User CurrentUser { get; set; }

        public UserManager()
        {
            //start out with 0 users
            Users = new ConcurrentDictionary<string, FullUser>();
        }

        public void SetupTimedJob(IHostCAALHPContract hostObj)
        {
            _container = new UnityContainer();
            Console.WriteLine("Registering types in Unity container...");
            _container.RegisterType<UserUpdateJob>(new InjectionConstructor(hostObj, this));
            _container.AddNewExtension<QuartzUnityExtension>();

            Console.WriteLine("Resolving IScheduler instance...");
            var scheduler = _container.Resolve<IScheduler>();

            Console.WriteLine("Scheduling job...");
            scheduler.ScheduleJob(
                new JobDetailImpl("UserUpdateJob", typeof(UserUpdateJob)),
                new CalendarIntervalTriggerImpl("UserUpdateJobTrigger", IntervalUnit.Minute, GetUserUpdateIntervalInMinutes()));

            Console.WriteLine("Starting scheduler...");
            scheduler.Start();
        }

        private static int GetUserUpdateIntervalInMinutes()
        {
            try
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["UserUpdateIntervalInMinutes"]);
            }
            catch (FormatException formatException)
            {
                Console.WriteLine(formatException);
                return 10;
            }
        }

        public void UpdateCripWithUsers(IHostCAALHPContract hostObj)
        {
            var userUpdater = new UserUpdater(hostObj, this);
            userUpdater.Run();
        }

    }
}