﻿using System.Collections.Concurrent;
using caalhp.Core.Contracts;
using caalhp.Core.Events.Types;

namespace UserLibrary.Managers
{
    public interface IUserManager
    {
        void UpdateCripWithUsers(IHostCAALHPContract hostObj);
        ConcurrentDictionary<string, FullUser> Users { get; set; }
        User CurrentUser { get; set; }
        void SetupTimedJob(IHostCAALHPContract hostObj);
    }
}