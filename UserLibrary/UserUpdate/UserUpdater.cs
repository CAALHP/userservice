﻿using System;
using caalhp.Core.Contracts;
using caalhp.Core.Utils.Helpers;
using UserLibrary.CRIP;
using UserLibrary.Managers;
using UserLibrary.Market;
using UserLibrary.UserUpdate.State;

namespace UserLibrary.UserUpdate
{
    public class UserUpdater
    {
        public IHostCAALHPContract Host { get; set; }
        public IServiceCAALHPContract ServiceImplementation { get; set; }
        public Guid CaalhpId { get; private set; }
        public IUserUpdateState State { get; set; }
        public ICrip CRIP { get; private set; }
        public IMarket Market { get; private set; }
        public bool Finished { get; set; }
        public IUserManager Manager { get; set; }

        public UserUpdater(IHostCAALHPContract host, IUserManager userManager)
        {
            Host = host;
            Manager = userManager;
            
            try
            {
                var id = CaalhpConfigHelper.GetCaalhpId();
                CaalhpId = id;
                //for testing while waiting for CRIP to function properly:
                CRIP = new Crip();
                Market = new Market.Market();
                Finished = false;
                State = new InitState();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Runs the update user flow
        /// </summary>
        public void Run()
        {
            while (!Finished)
            {
                State.Handle(this);
            }
        }
    }
}