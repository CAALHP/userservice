﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using MarketplaceContract;

namespace UserLibrary.UserUpdate.State
{
    public class IncompleteUserState : IUserUpdateState
    {
        private const int SleepPeriod = 500;

        public void Handle(UserUpdater context)
        {
            Console.WriteLine(GetType() + " getting biometrics from crip...");
            try
            {
                //  scan for new biometrics
                RequestFingerScanning(context);
                var bios = context.CRIP.GetNewBiometrics();
                ReportFingerWasScanned(context);
                //was scan successful?
                if (bios.Length == 2)
                {
                    //  Update user on market
                    var theUser = context.Manager.Users.Values.First(user => user.BaseUser.UserRole == UserRoleType.Citizen && user.BaseUser.Templates.Length == 0);
                    theUser.BaseUser.Templates = bios.Clone() as string[];
                    context.Market.UpdateUser(theUser.BaseUser);
                    //Get Users again
                    context.State = new InitState();
                }
                else
                {
                    Thread.Sleep(SleepPeriod);
                    context.State = new IncompleteUserState();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void ReportFingerWasScanned(UserUpdater context)
        {
            Console.WriteLine("Fingers were scanned!");
            var request = new ScanFingerResponseEvent
            {
                CallerName = ToString(),
                CallerProcessId = Process.GetCurrentProcess().Id
            };
            context.Host.ReportEvent(EventHelper.CreateEvent(SerializationType.Json, request));
        }

        private void RequestFingerScanning(UserUpdater context)
        {
            var request = new ScanFingerRequestEvent
            {
                CallerName = ToString(),
                CallerProcessId = Process.GetCurrentProcess().Id
            };
            context.Host.ReportEvent(EventHelper.CreateEvent(SerializationType.Json, request));
        }
    }
}