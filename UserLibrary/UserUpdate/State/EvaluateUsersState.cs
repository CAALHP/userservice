using System;
using System.Diagnostics;
using System.Linq;
using MarketplaceContract;

namespace UserLibrary.UserUpdate.State
{
    public class EvaluateUsersState : IUserUpdateState
    {
        public void Handle(UserUpdater context)
        {
            Console.WriteLine(GetType() + " evaluating users...");
            //Debugger.Launch();
            //If there is only 1 citizen with no biometrics, 
            var users = context.Manager.Users.Values;
            if (users.Any(user => user.BaseUser.UserRole == UserRoleType.Citizen && user.BaseUser.Templates.Length == 0))
            {
                //Prompt user for scanning a finger twice
                context.State = new IncompleteUserState();
            }
            else
            {
                context.State = new CompleteUsersState();
            }
        }
    }
}