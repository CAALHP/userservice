﻿namespace UserLibrary.UserUpdate.State
{
    public interface IUserUpdateState
    {
        void Handle(UserUpdater context);
    }
}