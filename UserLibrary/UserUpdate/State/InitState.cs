using System;
using System.Collections.Concurrent;
using UserLibrary.CRIP;
using UserLibrary.Managers;
using UserLibrary.Market;

namespace UserLibrary.UserUpdate.State
{
    public class InitState : IUserUpdateState
    {
        //private const int SleepPeriod = 5000;
        public void Handle(UserUpdater context)
        {
            Console.WriteLine(GetType() + " getting Users from market...");
            //Get Users
            //var users = _market.GetUsers(CaalhpId);
            try
            {
                UpdateUsersFromMarket(context.CaalhpId, context.Market, context.Manager);

                if (context.Manager.Users.Count == 0)
                    context.State = new InitState();
                else
                {
                    //Thread.Sleep(SleepPeriod);
                    context.State = new EvaluateUsersState();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void UpdateUsersFromMarket(Guid caalhpId, IMarket market, IUserManager manager)
        {
            try
            {
                var users = market.GetUsers(caalhpId);
                if (users == null) return;
                manager.Users = new ConcurrentDictionary<string, FullUser>();
                foreach (var user in users)
                {
                    var dto = new UserDTO(user);
                    var profile = market.GetUserProfile(user.UserId);
                    var fullUser = new FullUser(user, profile);
                    manager.Users.AddOrUpdate(dto.userid, fullUser, (key, olduser) => fullUser);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}