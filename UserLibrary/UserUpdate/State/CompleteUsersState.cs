﻿using System;
using System.Linq;

namespace UserLibrary.UserUpdate.State
{
    public class CompleteUsersState : IUserUpdateState
    {
        public void Handle(UserUpdater context)
        {
            Console.WriteLine(GetType() + " User update complete!");
            try
            {
                //Store templates on CRIP
                var users = context.Manager.Users.Values;
                context.CRIP.StoreTemplates(users);
                //the end
                context.Finished = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}