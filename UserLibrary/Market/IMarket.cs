﻿using System;
using MarketplaceContract;

namespace UserLibrary.Market
{
    public interface IMarket
    {
        User[] GetUsers(Guid id);
        void UpdateUser(User user);
        UserProfile GetUserProfile(string userId);
    }
}