﻿using System.Collections.Specialized;
using System.Configuration;
using caalhp.Core.Utils.Helpers;
using MarketplaceContract;
using Newtonsoft.Json;
using UserLibrary.Web;

namespace UserLibrary.Market
{
    using System;
    using System.Net;

    public class Market : IMarket
    {
        private readonly WebClient _webClient;
        private readonly Uri _baseAddress;

        public Market()
        {
            // web client
            _webClient = new CookieWebClient();

            _baseAddress = new Uri(ConfigurationManager.AppSettings.Get("MarketAddress"));

            // Login into the website (at this point the Cookie Container will do the rest of the job for us, and save the cookie for the next calls)

            AuthorizeCaalhp();
        }

        private void AuthorizeCaalhp()
        {
            var relativeUri = "api/caalhp/authorize/" + CaalhpConfigHelper.GetCaalhpId();
            var authUri = new Uri(_baseAddress, relativeUri);
            var nameValues = new NameValueCollection();
            _webClient.UploadValues(authUri, "POST", nameValues);
        }

        public User[] GetUsers(Guid id)
        {
            var address = new Uri(_baseAddress, "api/users/" + id);
            _webClient.Headers["Content-type"] = ContentType.Json;

            // invoke the REST method
            var json = _webClient.DownloadString(address);

            // deserialize from json
            var users = JsonConvert.DeserializeObject<User[]>(json);
            return users;
        }

        public void UpdateUser(User user)
        {
            var address = new Uri(_baseAddress, "api/users");
            _webClient.Headers["Content-type"] = ContentType.Json;

            //serialize to json
            var jsonUser = JsonConvert.SerializeObject(user);

            // invoke the REST method
            _webClient.UploadString(address, RestMethod.PUT, jsonUser);
        }

        public UserProfile GetUserProfile(string userId)
        {
            var address = new Uri(_baseAddress, "api/users/profile/" + userId);
            _webClient.Headers["Content-type"] = ContentType.Json;

            // invoke the REST method
            var json = _webClient.DownloadString(address);

            // deserialize from json
            var profile = JsonConvert.DeserializeObject<UserProfile>(json);
            return profile;
        }
    }
}