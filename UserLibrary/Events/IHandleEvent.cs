﻿namespace UserLibrary.Events
{
    public interface IHandleEvent
    {
        void HandleEvent(dynamic obj);
    }
}