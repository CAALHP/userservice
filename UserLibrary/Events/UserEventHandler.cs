﻿using System;
using System.Globalization;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization.JSON;
using crip2caalhp_events;
using UserLibrary.Managers;
using caalhp.Core.Utils.Helpers.Serialization;

namespace UserLibrary.Events
{
    public class UserEventHandler : IHandleEvent
    {
        private readonly IUserManager _userManager;
        private readonly caalhp.Core.Contracts.IHostCAALHPContract _host;

        public UserEventHandler(IUserManager userManager)
        {
            _userManager = userManager;
        }

        public UserEventHandler(IUserManager userManager, caalhp.Core.Contracts.IHostCAALHPContract hostCAALHPContract)
        {
            _userManager = userManager;
            _host = hostCAALHPContract;
        }

        public void HandleEvent(dynamic notification)
        {
            Console.WriteLine(notification);
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            Handle(obj);
        }

        private void Handle(GenericInfoEvent obj)
        {
            if (obj.Info.Equals("UpdateUsers"))
            {
                _userManager.UpdateCripWithUsers(_host);
            }
        }

        private void Handle(FoundIdFromCripEvent obj)
        {
            //if(_userManager.Users.Any(user => user.UserId.)
            //market.
            Console.WriteLine("AuthType/Id = " + obj.AuthType + "/" + obj.Id);
            FullUser user;
            if (!_userManager.Users.TryGetValue(obj.Id.ToString(CultureInfo.InvariantCulture), out user)) return;
            
            Console.WriteLine("Found User, id: " + user.BaseUser.UserId + " role: " + user.BaseUser.UserRole);
            Console.WriteLine("Found User, : " + user.Profile.FristName + " " + user.Profile.LastName);
            var userEvent = new UserLoggedInEvent
            {
                PhotoRoot = user.Profile.ProfilePhotoURL,
                User = new caalhp.Core.Events.Types.User 
                {
                    FirstName = user.Profile.FristName,
                    LastName = user.Profile.LastName, 
                    Id = user.Profile.UserId, 
                    Role = caalhp.Core.Events.Types.UserRole.Citizen 
                }
            };
            _userManager.CurrentUser = userEvent.User;
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, userEvent);
            _host.ReportEvent(serializedCommand);
        }

        private void Handle(CurrentUserRequestEvent obj)
        {
            var currentUserResponse = new CurrentUserResponseEvent
            {
                CurrentUser = _userManager.CurrentUser
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, currentUserResponse);
            _host.ReportEvent(serializedCommand);
        }

        private void Handle(UserLoggedOutEvent obj)
        {
            _userManager.CurrentUser = null;
            Console.WriteLine("User Logged Out");
        }

    }
}
