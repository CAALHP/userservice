﻿using System;
using System.Collections.Generic;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using crip2caalhp_events;
using UserLibrary.Events;
using UserLibrary.Managers;

namespace UserLibrary
{
    public class UserServiceImplementation : IServiceCAALHPContract
    {
        private const string Name = "UserService";
        private IHandleEvent _userEventHandler;
        private readonly IUserManager _userManager;

        public IServiceHostCAALHPContract ServiceHost { get; private set; }
        public int ProcessId { get; private set; }

        public UserServiceImplementation()
        {
            _userManager = new UserManager();
            
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            //var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            //dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            _userEventHandler.HandleEvent(notification);
        }

        public string GetName()
        {
            return Name;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            Console.WriteLine("Initializing UserService...");
            ServiceHost = hostObj;
            ProcessId = processId;
            _userEventHandler = new UserEventHandler(_userManager, hostObj.Host);
            Console.WriteLine("Setting up timed job for calling UpdateCripWithUsers()...");
            _userManager.SetupTimedJob(hostObj.Host);
            Console.WriteLine(GetType() + " Initialized.");
            
            ServiceHost.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FoundIdFromCripEvent),"crip2caalhp-events"), ProcessId);
            ServiceHost.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(GenericInfoEvent)), ProcessId);
            ServiceHost.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(CurrentUserRequestEvent)), ProcessId);
            ServiceHost.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedOutEvent)), ProcessId);
             
        }

        public void Start()
        {
            //throw new NotImplementedException();
        }

        public void Stop()
        {
            //throw new NotImplementedException();
        }
    }
}
