﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using UserLibrary.Managers;
using UserLibrary.Web;

namespace UserLibrary.CRIP
{
    public class Crip : ICrip
    {
        private readonly WebClient _webClient;
        private readonly Uri _baseAddress;
        
        //this internal class is only for serializing biometric templates.
        class TemplateArray
        { 
            public string[] templates;
        }

        public Crip()
        {
            // web client
            _webClient = new WebClient();
            _webClient.Headers["Content-type"] = ContentType.Json;
            _baseAddress = new Uri(ConfigurationManager.AppSettings.Get("CripAddress"));
        }

        public string[] GetNewBiometrics()
        {
            try
            {

                var address = new Uri(_baseAddress, "biometric/templates");

                // invoke the REST method
                var json = _webClient.DownloadString(address);

                // deserialize from json
                var biometrics = JsonConvert.DeserializeObject<TemplateArray>(json);
                return biometrics.templates;
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                    if ((e.Response as HttpWebResponse).StatusCode == HttpStatusCode.InternalServerError)
                        Console.WriteLine("failed to scan fingers within timeframe, try again!");
                    else throw;
                else throw;
            }
            return new string[0];
        }

        public void StoreTemplates(ICollection<FullUser> users)
        {
            //var address = new Uri(_baseAddress, "biometric/user");
            var userData = users.Select(user => new UserDTO(user.BaseUser))
                .Where(user => user.templates.Length == 2 && user.templates[0].Length == 768 && user.templates[1].Length == 768)
                .ToList();

            // deserialize from json
            //Debugger.Launch();
            Console.WriteLine("Storing " + userData.Count + " users into CRIP");
            foreach (var user in userData) 
            {
                Console.WriteLine("Storing user with id: " + user.userid + " into CRIP");
            }
            var jsonUsers = JsonConvert.SerializeObject(userData);

            // invoke the REST method
            //Since we are replacing the list in CRIP, this should be a PUT for living up to RESTful semantics.
            //_webClient.Headers["Content-type"] = "application/x-www-form-urlencoded";
            //_webClient.UploadString(address, RestMethod.POST, jsonUsers);
            StoreBiometricsInCrip(jsonUsers);
        }

        public void StoreBiometricsInCrip(string jsonStr)
        {
            HttpWebRequest request = CreateWebRequest(_baseAddress + "biometric/user");
            request.Method = "POST";
            request.ContentLength = jsonStr.Length;
            request.ContentType = "application/x-www-form-urlencoded";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(jsonStr);
                streamWriter.Flush();
                streamWriter.Close();
                try
                {
                    var httpResponse = (HttpWebResponse)request.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        if (httpResponse.StatusCode.ToString() == "200")
                        {
                            Console.WriteLine("Biometrics stored in crip");
                        }
                        else if (httpResponse.StatusCode.ToString() == "500")
                        {
                            Console.WriteLine("Something was not stored correctly in crip!");
                        }
                    }
                }
                catch (WebException e)
                {
                    if (e.Status == WebExceptionStatus.ProtocolError)
                        if ((e.Response as HttpWebResponse).StatusCode == HttpStatusCode.InternalServerError)
                            Console.WriteLine("Biometrics might have been stored in crip, but there were some issues!");
                        else throw;
                    else throw;
                }
            }
        }

        private HttpWebRequest CreateWebRequest(string endPoint)
        {
            var request = (HttpWebRequest)WebRequest.Create(endPoint);

            request.Method = "GET";
            request.ContentLength = 0;
            request.ContentType = "text/xml";

            return request;
        }

    }
}