﻿using MarketplaceContract;

namespace UserLibrary.CRIP
{
    public class UserDTO
    {
        public UserDTO(User user)
        {
            //var bytes = new Guid(user.UserId).ToByteArray();
            //userid = (bytes[0] + bytes[1] * 16).ToString(CultureInfo.InvariantCulture);
            userid = user.UserId;
            templates = user.Templates.Clone() as string[];
        }

        public string[] templates { get; set; }

        public string userid { get; set; }
    }
}
