﻿using System.Collections.Generic;
using MarketplaceContract;
using UserLibrary.Managers;

namespace UserLibrary.CRIP
{
    public interface ICrip
    {
        string[] GetNewBiometrics();
        void StoreTemplates(ICollection<FullUser> users);
    }
}