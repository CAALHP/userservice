﻿using System;
using caalhp.IcePluginAdapters;
using UserLibrary;

namespace UserService
{
    class Program
    {
        private static ServiceAdapter _adapter;
        private static UserServiceImplementation _implementation;

        static void Main(string[] args)
        {
            const string endpoint = "localhost";
            try
            {
                _implementation = new UserServiceImplementation();
                _adapter = new ServiceAdapter(endpoint, _implementation);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Press <ENTER> to exit program.");
            Console.ReadLine();
        }
    }
}
